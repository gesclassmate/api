const express = require('express');
const app = express();

require('./settings')(app);       console.log('>> Paramètres initialisés');
require('./models')(app);         console.log('>> Models initialisés');
require('./controller')(app);    console.log('>> Controller initialisés');
require('./services')(app);        console.log('>> Services initialisées');
require('./routes')(app);         console.log('>> Routes initialisées');

console.log(`Serveur démarré sur le port ${app.settings.port}.`)
app.listen(app.settings.port);
