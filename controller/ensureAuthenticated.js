const jwt = require('jsonwebtoken');

module.exports = (app) => {
    return (req, res, next) => {
        // Verify authorization header exists
        if (!req.headers || !req.headers.authorization) {
            return res.status(401).send('authentication.required');
        }

        const encryptedToken = req.headers.authorization;

        // Verify the token is valid
        jwt.verify(encryptedToken, app.settings.security.salt, null, (err, decryptedToken) => {
            if (err) {
                return res.status(401).send('invalid.token');
            }

            var connection = app.services.auth.connectSql();

            connection.query("SELECT * FROM user WHERE token = '" + encryptedToken + "'", function(err, rows){
                if(err){
                    connection.end();
                    return res.status(401).send('invalid.token');
                }

                if (!rows.length) {
                    connection.end();
                    return res.status(401).send('authentication.expired');
                }

                req.id_user = rows[0].id_user;
                connection.end();

                return next();
            });
        });
    };
};
