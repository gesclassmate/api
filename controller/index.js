module.exports = (app) => {
    app.controller = {
        ensureAuthenticated: require('./ensureAuthenticated')(app),
        bodyParser: require('body-parser')
    };
};
