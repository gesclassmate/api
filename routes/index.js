module.exports = (app) => {
  app.use('/auth', require('./auth')(app));
  app.use('/question/', require('./questions/questions')(app));
  app.use('/user/', require('./users/users')(app));
  app.use('/classroom/', require('./classrooms/classrooms')(app));
  app.use('/course/', require('./courses/courses')(app));
  app.use('/answer/', require('./answers/answers')(app));
  app.use('/media/', require('./multimedia/multimedia')(app));
};
