const router = require('express').Router();

module.exports = (app) => {
    router.get('/all',
        app.services.classrooms.getAll);

    return router;
}