const router = require('express').Router();

module.exports = (app) => {

    router.get('/:idMultimedia',
        app.services.media.get);

    router.get('/byCourse/:idCourse',
        app.services.media.getByCourse);

    router.post('/upload',
        app.controller.bodyParser.json(),
        app.services.media.upload);

    return router;
}