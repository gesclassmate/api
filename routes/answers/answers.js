const router = require('express').Router();

module.exports = (app) => {
    router.post('/create',
        app.controller.bodyParser.json(),
        app.services.answers.create);

    return router;
}