const router = require('express').Router();

module.exports = (app) => {
  router.post('/signin',
    app.controller.bodyParser.json(),
    app.services.auth.signIn);

  router.post('/logout',
    app.controller.bodyParser.json(),
    app.controller.ensureAuthenticated,
    app.services.auth.logOut);

  router.post('/token',
    app.controller.bodyParser.json(),
    app.controller.ensureAuthenticated,
    app.services.auth.getToken)
    
  return router;
}
