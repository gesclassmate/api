const router = require('express').Router();

module.exports = (app) => {
    router.get('/all',
        app.services.courses.getAll);

    router.post('/mycourses',
        app.controller.bodyParser.json(),
        app.services.courses.myCourses);

    return router;
}