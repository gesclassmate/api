const router = require('express').Router();

module.exports = (app) => {
    router.get('/:idUser',
        app.services.users.myProfile);

    router.get('/all',
        app.services.users.getAll);

    router.get('/token/:token',
    	app.services.users.getUserIdByToken);


    return router;
}