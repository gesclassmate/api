module.exports = (app) => {
  function getAll(req, res, next){
    var connection = app.services.auth.connectSql();
    
    connection.query("SELECT * FROM courses", function(err, rows) {
      if(err){
        console.log(err);
        connection.end();
        return res.status(500).send();
      }
      if(rows.length) {
        connection.end();
        return res.status(200).send(rows);

      } else {
          console.log("no data found");
          connection.end();
          return res.status(404).send();
      }
    });
  }

  function myCourses(req, res,next){
    var connection = app.services.auth.connectSql();

    connection.query("SELECT courses.idCourse, courses.Subject, courses.Room, courses.Date FROM courses, classroom, profile, user WHERE courses.idClassroom = classroom.idClassroom AND classroom.idClassroom = profile.idClassroom AND profile.idUser = user.idUser AND user.token = '" + req.body.token + "'", function(err, rows) {
      if(err){
        console.log(err);
        connection.end();
        return res.status(500).send();
      }
      if(rows.length) {
        connection.end();
        return res.status(200).send(rows);

      } else {
          console.log("no data found");
          connection.end();
          return res.status(404).send();
      }
    });
  }

  return {
    getAll,
    myCourses
  };
};
    