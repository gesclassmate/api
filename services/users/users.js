module.exports = (app) => {

function getUserIdByToken(req, res,next){
    var connection = app.services.auth.connectSql();

    connection.query("SELECT user.idUser FROM user WHERE token = '"+ req.params.token +"'", function(err, rows) {
      if(err){
        console.log(err);
        connection.end();
        return res.status(500).send();
      }
      if(rows.length) {
        connection.end();
        return res.status(200).send(rows);

      } else {
          console.log("no data found");
          connection.end();
          return res.status(404).send();
      }
    });
  }

function myProfile(req, res,next){
    var connection = app.services.auth.connectSql();

    connection.query("SELECT profile.Firstname, profile.Lastname, profile.School, profile.Points FROM profile WHERE idUser = '"+ req.params.idUser +"'", function(err, rows) {
      if(err){
        console.log(err);
        connection.end();
        return res.status(500).send();
      }
      if(rows.length) {
        connection.end();
        return res.status(200).send(rows);

      } else {
          console.log("no data found");
          connection.end();
          return res.status(404).send();
      }
    });
  }

    function get(req, res, next){
      var connection = app.services.auth.connectSql();

      connection.query("SELECT * FROM profile WHERE idUser = '"+ req.params.idUser +"'", function(err, rows, fields) {
        if(err){
          console.log(err);
          connection.end();
          return res.status(500).send();
        }
        if(rows.length) {
          connection.end();
          return res.status(200).send(rows);

        } else {
            console.log("no data found");
            connection.end();
            return res.status(404).send();
        }
      });
    }

    function getAll(req, res, next){
      var connection = app.services.auth.connectSql();
      
      connection.query("SELECT * FROM profile", function(err, rows, fields) {
        if(err){
          console.log(err);
          connection.end();
          return res.status(500).send();
        }
        if(rows.length) {
          connection.end();
          return res.status(200).send(rows);
  
        } else {
            console.log("no data found");
            connection.end();
            return res.status(404).send();
        }
      });
    }

    return {
      myProfile,
      getAll,
      getUserIdByToken
    };
};
