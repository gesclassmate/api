module.exports = (app) => {
    
    function getAll(req, res, next){
      var connection = app.services.auth.connectSql();
      
      connection.query("SELECT * FROM classroom", function(err, rows, fields) {
        if(err){
          console.log(err);
          connection.end();
          return res.status(500).send();
        }
        if(rows.length) {
          connection.end();
          return res.status(200).send(rows);
  
        } else {
            console.log("no data found");
            connection.end();
            return res.status(404).send();
        }
      });
    }

    return {
      getAll
    };
};
    