const jwt = require('jsonwebtoken');

module.exports = (app) => {
    function connectSql() {
        var connection = app.models.mysql.createConnection(app.settings.db.sql, function(err){
        console.log(err);
      });

      console.log("mysql...");
      connection.connect(function(err) {
          if ( !err ) {
            console.log("Connected to MySQL");
          } else if ( err ) {
            console.log(err);
          }
      });

      return connection;
    }

    function signIn(req, res, next) {
        var connection = connectSql();

        var user = {
            id_user: 0,
            username: req.body.username,
            password: req.body.password
        };

        connection.query("SELECT * FROM user WHERE Username = '" + user.username + "' AND Password = '" + user.password + "'", function (err, rows) {
            if (err) {
                connection.end();
                return res.status(500).send(err);
            }

            // No user found with this credentials.
            if (!rows.length) {
                connection.end();
                return res.status(401).send('invalid.credentials');
            }

            user.id_user = rows[0].idUser;

            // Encrypting the token with JWT convention.
            jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (60 * 60) * 24 * 7,
                    tokenId: user.id_user.toString() + user.pseudo
                },
                app.settings.security.salt,
                {},
                (err, encryptedToken) => {
                    if (err) {
                        connection.end();
                        return res.status(500).send(err);
                    }

                    connection.query("UPDATE user SET token = '" + encryptedToken + "' WHERE idUser = '" + user.id_user.toString() + "'",function (err, result) {
                        if (err) {
                            connection.end();
                            return res.status(500).send(err);
                        }
                    });

                    connection.end();

                    // Sending the encrypted token.
                    return res.send(encryptedToken);
                }
            );
        });
    }

    function logOut(req, res, next) {
        var connection = connectSql();

        connection.query("UPDATE user SET token = '' WHERE idUser = '" + req.id_user + "'", function(err, rows){
            if (err) {
                connection.end();
                return res.status(500).send(err);
            }

            // No user found
            if (!rows.affectedRows) {
                connection.end();
                return res.status(401).send('invalid.credentials');
            }

            connection.end();
            return res.send('Disconnected');
        });
    }

    function getToken(req, res, next){
        var connection = connectSql();

        connection.query("SELECT token FROM user WHERE token = '" + req.token + "'", function(err, rows){
            if (err) {
                connection.end();
                return res.status(500).send(err);
            }

            // No user found with this credentials.
            if (!rows.length) {
                connection.end();
                return res.status(401).send('invalid.credentials');
            }

            res.status(200).send();
        })
    }

    
    return {
      signIn,
      logOut,
      connectSql,
      getToken
    };
};