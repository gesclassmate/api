module.exports = (app) => {
    app.services = {
        auth: require('./auth')(app),
        questions: require('./questions/questions')(app),
        users: require('./users/users')(app),
        classrooms: require('./classrooms/classrooms')(app),
        courses: require('./courses/courses')(app),
        answers: require('./answers/answers')(app),
        media: require('./multimedia/multimedia')(app)
    };
};
