const fileUpload = require('express-fileupload');

module.exports = (app) => {
    function upload(req, res, next){
        if (!req.files)
            return res.status(400).send('No files were uploaded.');
         
        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
        let sampleFile = req.files.sampleFile;
        
        // Use the mv() method to place the file somewhere on your server
        sampleFile.mv(req.body.title, function(err) {
            if (err)
                return res.status(500).send(err);
            
            var connection = app.services.auth.connectSql();
            
            connection.query("INSERT INTO multimedias (idCourse, Type, Source) VALUES ('" + req.body.idCourse + "', 'audio','" + req.body.title + "')", function(err, result){
                if(err) {
                    console.log(err);
                    connection.end();
                    return res.status(500).send(err);
                }
                if(result.affectedRows == 0){
                    connection.end();
                    return res.status(520).send("no data insert");
                } if(result.affectedRows == 1){
                    var newIdUser = result.insertId;
                    connection.end();
                    return res.status(201).send(newIdUser);
                }
            });
        });
    }

    function get(req, res, next){
        var connection = app.services.auth.connectSql();
        
        connection.query("SELECT * FROM multimedias WHERE idMultimedia = '" + req.params.idMultimedia + "'", function(err, rows, fields) {
            if(err){
                console.log(err);
                connection.end();
                return res.status(500).send();
            }
            if(rows.length) {
                connection.end();
                return res.status(200).send(rows);

            } else {
                console.log("no data found");
                connection.end();
                return res.status(404).send();
            }
        });
    }

    function getByCourse(req, res, next){
        var connection = app.services.auth.connectSql();
        
        connection.query("SELECT * FROM multimedias WHERE idCourse = '" + req.params.idCourse + "'", function(err, rows, fields) {
            if(err){
                console.log(err);
                connection.end();
                return res.status(500).send();
            }
            if(rows.length) {
                connection.end();
                return res.status(200).send(rows);

            } else {
                console.log("no data found");
                connection.end();
                return res.status(404).send();
            }
        });
    }

    return {
        upload,
        get,
        getByCourse
    };
}